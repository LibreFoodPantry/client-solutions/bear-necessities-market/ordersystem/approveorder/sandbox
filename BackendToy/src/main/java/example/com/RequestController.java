package example.com;

import example.com.Exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class RequestController {
    // GET - Retrieve information from a Rest API
    @GetMapping("/get/books")
    public ResponseEntity<List<Book>> findAllBooks() {
        List<Book> books = BookService.findAll();
        return ResponseEntity.ok(books);  // return 200 (OK)
    }

    @GetMapping("/get/books/{bookId}")
    public ResponseEntity<Book> findBookById(@PathVariable long bookId) {
        try {
            Book book = BookService.findById(bookId);
            return ResponseEntity.ok(book);  // return 200 (OK)
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null); // return 404, with null body
        }
    }

    // POST - Create new resource
    @PostMapping("/post/books")
    public ResponseEntity<Book> addBook(@RequestBody Book book) throws URISyntaxException {
        try {
            Book newBook = BookService.save(book);
            return ResponseEntity.created(new URI("/books/" + newBook.getId()))
                    .body(book);
        } catch (ResourceAlreadyExistsException ex) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    // DELETE
    @DeleteMapping(path="/delete/books/{bookId}")
    public ResponseEntity<Void> deleteBookById(@PathVariable long bookId) {
        try {
            BookService.deleteById(bookId);
            return ResponseEntity.ok().build();
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.notFound().build();
        }
    }
}

