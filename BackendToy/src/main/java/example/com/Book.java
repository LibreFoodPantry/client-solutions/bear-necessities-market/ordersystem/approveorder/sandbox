package example.com;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Book {
    private @Id final long id;
    private final String title;

    public Book(long id, String content) {
        this.id = id;
        this.title = content;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
