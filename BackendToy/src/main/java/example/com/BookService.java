package example.com;

import example.com.Exceptions.ResourceAlreadyExistsException;
import example.com.Exceptions.ResourceNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class BookService {
    private static List<Book> books = new ArrayList<>();

    public BookService() {
        books.add(new Book(1, "The Wizard of Oz"));
        books.add(new Book(2, "The Hobbit"));
        books.add(new Book(3, "The Hunger Games"));
    }

    public static List<Book> findAll() {
        return books;
    }

    public static Book findById(long Id) throws ResourceNotFoundException {
        for (Book b: books) {
            if (b.getId() == Id) return b;
        }
        throw new ResourceNotFoundException();
    }

    public static Book save(Book book) throws ResourceAlreadyExistsException {
        for (Book b: books) {
            if (b == book) {
                throw new ResourceAlreadyExistsException();
            }
        }
        books.add(book);
        return book;
    }

    public static void deleteById(long bookId) throws ResourceNotFoundException {
        for (Book b: books) {
            if (b.getId() == bookId) {
                books.remove(b);
                return;
            }
        }
        throw new ResourceNotFoundException();
    }
}
