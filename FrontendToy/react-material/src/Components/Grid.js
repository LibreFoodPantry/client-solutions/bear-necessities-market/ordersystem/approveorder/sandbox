import Grid from '@material-ui/core/Grid';
import React from 'react';

export default class List extends React.Component {
    render() {
        return (
            <div>
                <Grid>
                    <div class="grid-container">
                        <div class="grid-item">OR12390: lebronjames@gmail.com</div>
                        <div class="button1">Verify</div>
                        <div class="button2">Delete</div>
                        <div class="grid-item">OR12838: jm342193@wne.edu</div>
                        <div class="button1">Verify</div>
                        <div class="button2">Delete</div>
                        <div class="grid-item">OR12432: cg356266@wne.edu</div>
                        <div class="button1">Verify</div>
                        <div class="button2">Delete</div>
                        <div class="grid-item">OR16234: cdreyer@wne.edu</div>
                        <div class="button1">Verify</div>
                        <div class="button2">Delete</div>
                        <div class="grid-item">OR12212: stoneyjackson@wne.edu</div>
                        <div class="button1">Verify</div>
                        <div class="button2">Delete</div>
                    </div>
                </Grid>
            </div>
        );
    }
}